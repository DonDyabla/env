"use strict";

var env = require('../../..').create(__dirname);

var mailer = env.mailer;

return mailer({
    templateName: 'added-to-room',
    subject: 'Hi There',
    to: 'andrew@gitter.im',
    data: {
      FIRST_NAME: 'Mike'
    }
  })
  .then(function(result) {
    console.log(result);
  })
  .catch(function(err) {
    console.log(err.stack);
  });
