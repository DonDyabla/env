"use strict";

var assert = require('assert');
var RedisController = require('./redis-controller');
var debug = require('debug')('gitter:env:test:redis-spec');

module.exports = function redisSpec(create, quit) {
  var redisController;

  before(function(done) {
    redisController = new RedisController();
    done();
  });

  beforeEach(function() {
    redisController.ensureAllStarted();
  });

  after(function(done) {
    redisController.cleanup();
    done();
  });

  describe('failover', function() {
    this.timeout(15000);
    var mainRedisClient;

    beforeEach(function() {
      mainRedisClient = create(redisController.options);
    });

    afterEach(function(done) {
      quit(mainRedisClient);
      setTimeout(done, 500)
    });

    it('should obtain a handle failover correctly', function(done) {
      var setValue = String(Math.random());

      mainRedisClient.set('moo', setValue, function(err) {
        if(err) return done(err);

        redisController.stop('redis1');

        mainRedisClient.get('moo', function(err, value) {
          redisController.ensureAllStarted();

          if(err) return done(err);

          assert.strictEqual(value, setValue);

          done();
        });

      });

    });

    it('should handle mega failure correctly', function(done) {
      var setValue = String(Math.random());

      debug('setting value');
      mainRedisClient.set('s', setValue, function(err) {
        if(err) return done(err);

        // Kill the sentinel and the redis
        debug('terminating sentinel1 and redis1');
        redisController.stop('sentinel1');
        redisController.stop('redis1');

        setTimeout(function() {
          redisController.ensureAllStarted();

          function next() {
            debug('trying to read value again');

            mainRedisClient.get('s', function(err) {
              if (err) {
                debug('failed with: %s', err.stack || err)
                setTimeout(next, 500);
                return;
              }

              done();
            });
          }

          next();
        }, 1500);

      });

    });

    it('should handle sentinel failure correctly', function(done) {
      var setValue = String(Math.random());

      mainRedisClient.set('s', setValue, function(err) {
        if(err) return done(err);

        redisController.stop('sentinel1');

        setTimeout(function() {
          mainRedisClient.get('s', function(err, value) {
            if(err) return done(err);

            assert.strictEqual(value, setValue);
            done();
          });

        }, 500);

      });
    });

    it('should handle sentinel failure prior to initial connection', function(done) {
      redisController.stop('sentinel1');
      redisController.stop('sentinel2');
      redisController.stop('sentinel3');

      setTimeout(function() {
        redisController.ensureAllStarted();

        var setValue = String(Math.random());

        mainRedisClient.set('s', setValue, function(err) {
          if(err) return done(err);

          mainRedisClient.get('s', function(err, value) {
            if(err) return done(err);

            assert.strictEqual(value, setValue);
            done();
          });

        });

      }, 1000);

    });

  });

};
