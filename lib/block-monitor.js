/*jshint node:true, unused: true */
"use strict";

var blocked = require('blocked');

exports.create = function(options) {
  var config = options.config;

  if(config.get('blockMonitor:enabled') === false) return;

  function installBlocked() {

    var statsdClient = require('./stats-client').create({
      config: config,
      includeNodeVersionTags: true
    });

    blocked(function(ms) {
      statsdClient.histogram('eventloop.block', ms);
    });
  }

  // Give the process 5seconds to start before reporting blockages
  var startupDelay = config.get('blockMonitor:startupDelay') || 5000;
  setTimeout(installBlocked, startupDelay);

};
