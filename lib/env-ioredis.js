'use strict';

exports.create = function(options) {
  var Redis = require('ioredis');
  var redisUrlParser = require('./utils/redis-url-parser');
  var _ = require('lodash');
  var shutdown = require('shutdown');

  var logger = options.logger;
  var config = options.config;

  function connectionToIoRedis(connection, extras) {
    var result = {};
    if (connection.sentinel) {
      /* ioredis will always try redis sentinels from first to last
         so shuffle the list to ensure that no sentinel is slacking */
      var sentinels = _.shuffle(connection.sentinel.hosts)
                            .map(function(hostString) {
                              var s = hostString.split(':');
                              var port = parseInt(s[1], 10) || 26379;
                              return { host: s[0], port: port };
                            });

      result.sentinels = sentinels;
      result.name = connection.sentinel["master-name"];
    }

    if (connection.redisDb) {
      result.db = connection.redisDb;
    }

    if (connection.host) {
      result.host = connection.host;
    }

    if (connection.port) {
      result.port = connection.port;
    }

    if (extras) {
      _.extend(result, extras);
    }

    if (process.env.NODE_ENV === 'dev') {
      // Super useful stacktraces through Redis
      result.showFriendlyErrorStack = true;
    }

    return result;
  }

  var redisClients = [];

  shutdown.addHandler('redis', 1, function(callback) {
    function next() {
      if (!redisClients.length) return;
      var client = redisClients.pop();
      return client.quit()
        .catch(function(err) {
          logger.error('Error while quitting redis client: ' + err, { exception: err });
        })
        .then(next);
    }

    next().nodeify(callback);
  });

  return {

    // Create a non-singleton client
    createClient: function(connection, extras) {
      connection = connection || process.env.REDIS_CONNECTION_STRING || config.get('redis');
      if (typeof connection === 'string') {
        connection = redisUrlParser(connection);
      }
      var redisConnection = connectionToIoRedis(connection, extras);

      var redis = new Redis(redisConnection);
      redis.on('error', function(err) {
        logger.error('Redis error (ioredis): ' + err, { exception: err });
      });

      redisClients.push(redis);
      return redis;
    },

    quitClient: function(client) {
      return client.quit().then(function() {
        var index = redisClients.indexOf(client);
        if (index >= 0) {
          redisClients.splice(index, 1);
        }
      });
    },

    parse: function(connectionString) {
      return redisUrlParser(connectionString);
    }
  };
};
