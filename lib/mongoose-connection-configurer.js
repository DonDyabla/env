'use strict';

var mongodbConnString = require('mongodb-connection-string');
var mongodbArbiterDiscover = require('mongodb-arbiter-discovery');
var mongoDogStats = require('mongodb-datadog-stats');
var shutdown = require('shutdown');


module.exports = function mongooseConnectionConfigurer(options) {
  var mongoose = options.mongoose;
  var config = options.config;
  var logger = options.logger;
  var env = options.env;

  var connection = mongoose.connection;

  /* Install dogstats */
  mongoDogStats.install(mongoose.mongo, {
    statsClient: env.createStatsClient({ includeNodeVersionTags: true }),
    sampleRate: 0.5
  });

  var mongoConnection = config.get("mongo:connection");
  var mongooseConnectionUrl = config.get("mongo:url");

  var autoIndex;
  if (config.get('mongo:noAutoIndex')) {
    autoIndex = false;
  } else {
    autoIndex = true;
  }

  var mongooseConnectOptions = {
    server: {
      keepAlive: 1,
      auto_reconnect: true,
      socketOptions: { keepAlive: 1, connectTimeoutMS: 60000 }
    },
    replset: {
      keepAlive: 1,
      auto_reconnect: true,
      socketOptions: { keepAlive: 1, connectTimeoutMS: 60000 }
    },
    config: {
      autoIndex: autoIndex
    }
  };


  if (mongoConnection) {
    mongooseConnectionUrl = mongodbConnString.mongo(mongoConnection);
  }
  mongoose.connect(mongooseConnectionUrl, mongooseConnectOptions);

  shutdown.addHandler('mongo', 1, function(callback) {
    mongoose.disconnect(callback);
  });

  var hasConnected = false;

  mongoose.connection.once('open', function() {
    hasConnected = true;
  });

  var autodiscoveryAttempts = 0;

  function attemptAutoDiscoveryConnection(err) {
    logger.error('Attempting autodiscovery as connection "' + mongoConnection + '" failed');

    function die(err) {
      console.error(err);
      console.error(err.stack);
      shutdown.shutdownGracefully(1);
    }

    if(autodiscoveryAttempts > 0) {
      logger.error('No autodiscovery already failed. Unable to continue.');
      /* Shutdown */
      return die(err);
    }

    autodiscoveryAttempts++;

    var autoDiscoveryHost = config.get('mongo:autoDiscovery:host');
    var autoDiscoveryPort = config.get('mongo:autoDiscovery:port') || 27017;
    var replicaSet = mongoConnection && mongoConnection.options && mongoConnection.options.replicaSet;

    if (!autoDiscoveryHost) {
      /* Autodiscovery not configured */
      logger.error('No autodiscovery host available, unable to autodiscover');
      return die(err);
    }

    logger.info('Autodiscovery via ' + autoDiscoveryHost + ":" + autoDiscoveryPort);

    mongodbArbiterDiscover({
      host: autoDiscoveryHost,
      port: autoDiscoveryPort,
      mongo: mongoose.mongo,
      replicaSet: replicaSet
    }, function(err, hosts) {
      if(err) return die(err);

      mongoConnection.hosts = hosts;
      mongoose.connect(mongodbConnString.mongo(mongoConnection), mongooseConnectOptions);
    });
  }

  connection.once('error', function(err) {
    /* Only attempt auto-discovery if we haven't already connected */
    if (hasConnected) return;

    logger.error("MongoDB connection error", { exception: err });
    attemptAutoDiscoveryConnection(err);
  });

};
