'use strict';

exports.create = function(options) {
  var config = options.config;

  if (config.get('amazonses:accessKeyId')) {
    return require('./mailers/amazon-ses').create(options);
  }

  if (config.get('mandrill:apiKey')) {
    return require('./mailers/mandrill').create(options);
  }

  return require('./mailers/fake-mailer').create(options);
};
