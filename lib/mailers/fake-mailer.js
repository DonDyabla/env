'use strict';

var Promise = require('bluebird');

function fakeMailer() {
  return Promise.resolve({ fake: true });
}

exports.create = function() {
  return fakeMailer;
};
